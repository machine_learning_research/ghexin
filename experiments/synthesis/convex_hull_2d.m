function [x, y] = convex_hull_2d(leaves)

check = zeros(length(leaves), 1);
lc = {check}
i = 1;
[x, y] = convex_hull_coords(leaves, check, i);

end
    

function [x, y] = convex_hull_coords(leaves, check, i)

    check(i) = 1;
    coord = leaves(i).RefVector.coordinates;
    x = coord(1);
    y = coord(2);

    for j = 1 : leaves(i).NeighboursSize
        leaf = 
        if check(j) == 0
            [x1, y1] = convex_hull_coords(leaves, check, i);
            x = [x x1];
            y = [y y1];
        end
    end
end